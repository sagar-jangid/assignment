# This class is used to reverse a list using the functionality of iterators


class IteratorReverse:

    # this is a constructor that will initialize the things

    def __init__(self, user_input_list):
        if type(user_input_list) is list:
            self.user_input_list = user_input_list
        else:
            self.user_input_list = [user_input_list]

        self.end = len(self.user_input_list)
        self.initialize = 0

    # This will return iterator object itself after some initialization

    def __iter__(self):
        self.user_input_list.reverse()
        return self

    # This method will in turn produce the next item in the list

    def next(self):
        initialize = self.initialize
        try:
            if initialize < self.end:
                self.initialize += 1
                return self.user_input_list[initialize]

            else:
                raise StopIteration()

        except StopIteration:
            print "The List Is fully Iterated and reached to end !!"

if __name__ == '__main__':
    print "Please enter the entries list seperated using ',' :"
    user_input_list = raw_input().split(',')
    print user_input_list
    iterator_reverse = IteratorReverse(user_input_list)
    iterator_reverse.__iter__()
    ch = '1'
    while ch == '1':
        print iterator_reverse.next()
        print "Enter 1 to see more entries else press any other key!!"
        ch = raw_input()

