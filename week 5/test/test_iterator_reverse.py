# This class is used to test the program of reversing list using iterator


import unittest
from LearningPlan.iterator_reverse import IteratorReverse


class TestIteratorReverse(unittest.TestCase):

    # this method will test object creation
    def test_create_object_using_int(self):
        iterator_reverse = IteratorReverse(1)
        self.assertEqual(len(iterator_reverse.user_input_list), 1)
        self.assertEqual(iterator_reverse.user_input_list[0], 1)

        with self.assertRaises(IndexError):
            iterator_reverse.user_input_list[2]

    # this method will test object creation using string
    def test_create_object_using_string(self):
        iterator_reverse = IteratorReverse("1")
        self.assertEqual(len(iterator_reverse.user_input_list), 1)
        self.assertEqual(iterator_reverse.user_input_list[0], "1")

        with self.assertRaises(IndexError):
            iterator_reverse.user_input_list[2]

    # this method will test object creation using list
    def test_create_object_using_list(self):
        iterator_reverse = IteratorReverse([1 ,2, 3, 4, 5])
        iterator_reverse1 = IteratorReverse(['a', 'b', 'c', 'd', 'e'])
        with self.assertRaises(IndexError):
            iterator_reverse.user_input_list[5]
            iterator_reverse1.user_input_list[8]

    # this method will test working and returning results
    def test_working(self):
        iterator_reverse1 = IteratorReverse(['a', 'b', 'c', 'd', 'e'])
        iterator_reverse1.__iter__()
        self.assertEqual('e',iterator_reverse1.next())
        self.assertEqual('d', iterator_reverse1.next())
        self.assertEqual('c', iterator_reverse1.next())
        self.assertEqual('b', iterator_reverse1.next())
        self.assertEqual('a', iterator_reverse1.next())
        self.assertRaises(StopIteration, iterator_reverse1.next())
