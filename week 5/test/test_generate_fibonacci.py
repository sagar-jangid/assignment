# This class will test the program of fibonacci series generator
import unittest
from LearningPlan.generate_fibonacci import GenerateFibonacciSeries

"""
To do ://  When a negative integer is entered it should not accept the value and should return an error msg
"""

class TestGenerateFibonacciSeries(unittest.TestCase):

    # This method will test creation of a object

    def test_create_object(self):
        with self.assertRaises(ValueError):
            GenerateFibonacciSeries(-1)
        GenerateFibonacciSeries(3)
        #with self.assertRaises(ValueError):
            #GenerateFibonacciSeries('a')
        with self.assertRaises(TypeError):
            GenerateFibonacciSeries()


    # This method will test whether the value errror is raised when we put wrong value or not

    # def test_raise_valueerror(self):
        #    self.assertRaises(ValueError,GenerateFibonacciSeries(9)) # Python evaluation is strict,
        # which means that when evaluating the above expression, it will first evaluate all
        # the arguments, and after evaluate the method call.
        # self.assertRaises(TypeError, GenerateFibonacciSeries, 'a')

    # This method will test that the output generated is according to the need or not

    def test_working(self):
        generate_fibonacci_series = GenerateFibonacciSeries('5')
        output_list=[value for value in generate_fibonacci_series.generate_fibonacci()]
        self.assertEqual(0, output_list[0])
        self.assertEqual(1, output_list[1])
        self.assertEqual(1, output_list[2])
        self.assertEqual(2, output_list[3])
        self.assertEqual(3, output_list[4])
        self.assertEqual(5, output_list[5])
        self.assertEqual(8, output_list[6])
        self.assertEqual(13, output_list[7])


        with self.assertRaises(IndexError):
            output_list[8]