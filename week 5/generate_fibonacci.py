# program to write a generator for fibonacci number


class GenerateFibonacciSeries:

    # This is a initializer that will initialize the required variables

    def __init__(self, end):

        self.first_number = 0
        self.second_number = 1
        self.begin = 0
        self.end = end

        if end < 0:
            raise ValueError

        while True:
            try:
                self.end = int(self.end)
            except ValueError:
                print("No valid number! Please try again ...")
                self.end = raw_input()
                continue
            else:
                break


    # This method is a generator that will yield the result of fabonacci series

    def generate_fibonacci(self):

        yield self.first_number
        yield self.second_number
        while self.begin <= self.end:
            self.first_number, self.second_number = self.second_number, self.first_number + self.second_number
            yield self.second_number
            self.begin += 1


if __name__ == '__main__':

    print "Please enter the end point of range to compute :"
    end = raw_input()
    generatefibonacci = GenerateFibonacciSeries(end)
    fib = generatefibonacci.generate_fibonacci()
    ch = '1'
    while ch == '1':
        try:
            print "The next item in Fibonacci series is: ", next(fib)
        except Exception as e:
            print "The Iteration has to be stopped as the range is over!!!"

        print ('Enter 1 to produce next result else any key!!')
        ch = raw_input()
