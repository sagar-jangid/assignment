class Engine:
    def setPower(self,pow):
        self.pow=pow
        return pow

    def setFD(self,fd):
        self.FD=fd
        return fd

    def setDisplacement(self,displacement):
        self.displacement=displacement
        return displacement

    def setIgnition(self,ign):
        self.ign=ign
        return ign

    def start(self):
        print ("Start Bike Using Ignition")

    def stop(self):
        print("Stop fuel supply\n\n")

    def slowDown(self):
        print("decrease fuel supply along with gear down")

    def speedUp(self):
        print ("increase Fuel amount and gear up")

class Bike(Engine):
    def setManufacturer(self, man):
        self.man=man
        return man

    def setModelName(self,mn ):
        self.mn=mn
        return mn

    def setprice(self,price):
        self.price=price
        return price

passion=Bike()
print " The Manufacturer of the Bike is :",passion.setManufacturer("Hero")
print " The Model of the Bike is :",passion.setModelName("Passion")
print " The Price of the Bike is :",passion.setprice(48950)
print " The Power of the Engine is :",passion.setPower("8000 rpm")
print " The Power of the fuel delivery is through :",passion.setFD("Carburettor")
print " The displacement of the Engine is :",passion.setDisplacement("97 cc")
print " The ignition system of the Engine is :",passion.setIgnition("Digital CDI")
passion.start()
passion.speedUp()
passion.slowDown()
passion.stop()

r15=Bike()
print " \n\nThe Manufacturer of the Bike is :",r15.setManufacturer("Yamaha")
print " The Model of the Bike is :",r15.setModelName("YZF R15")
print " The Price of the Bike is :",r15.setprice(125000)
print " The Power of the Engine is :",r15.setPower("10000 rpm")
print " The Power of the fuel delivery is through :",r15.setFD("Fuel Injection")
print " The displacement of the Engine is :",r15.setDisplacement("155 cc")
print " The ignition system of the Engine is :",r15.setIgnition("Transistor Controlled Ignition")
r15.start()
r15.speedUp()
r15.slowDown()
r15.stop()
