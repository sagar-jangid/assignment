# This class defines some methods to call the database for CRUD operations
# This class removes many redundancies from the code


class Model:

    def __init__(self):
        pass

    def retrieve_data_using_select(self, table, **kwargs):
        """ Generates SQL for a SELECT statement
        can be used with condition matching the
         kwargs or can be used to select whole data"""

        sql = list()
        sql.append("SELECT * FROM %s " % table)
        if kwargs:
            sql.append("WHERE " + " AND ".join("%s = '%s'" % (k, v) for k, v in kwargs.iteritems()))
        sql.append(";")
        return "".join(sql)

    def insert_new_entry(self, table, **kwargs):
        """ insert rows into table given the data as
         key-value pairs in kwargs """

        keys = ["%s" % k for k in kwargs]
        values = ["'%s'" % v for v in kwargs.values()]
        sql = list()
        sql.append("INSERT INTO %s (" % table)
        sql.append(", ".join(keys))
        sql.append(") VALUES (")
        sql.append(", ".join(values))
        sql.append(")")
        # sql.append(") ON DUPLICATE KEY UPDATE ")
        # sql.append(", ".join("%s = '%s'" % (k, v) for k, v in kwargs.iteritems()))
        sql.append(";")
        return "".join(sql)

    def insert_new_entry_using_args(self, table, *args):
        """ insert rows into table given the data as
                values only in args """

        values = ["'%s'" % v for v in args]
        sql = list()
        sql.append("INSERT INTO %s VALUES (" % table)
        sql.append(", ".join(values))
        sql.append(")")
        # sql.append(") ON DUPLICATE KEY UPDATE ")
        # sql.append(", ".join("%s = '%s'" % (k, v) for k, v in kwargs.iteritems()))
        sql.append(";")
        return "".join(sql)

    def delete_specific_record(self, table, **kwargs):
        """ deletes rows from table using the where
        clause checks for **kwargs match """

        sql = list()
        sql.append("DELETE FROM %s " % table)
        sql.append("WHERE " + " AND ".join("%s = '%s'" % (k, v) for k, v in kwargs.iteritems()))
        sql.append(";")
        return "".join(sql)
