# This class will take care of issuing book to the student
# All the functionality related to book issue is covered here

import datetime
from settings.database_connection import DatabaseConnection
from models.model import Model


class IssueBook(DatabaseConnection):

    # This will do all initializations
    def __init__(self):
        self.book_id = ''
        self.st_id = ''
        self.instances = 0
        self.cards = 0
        self.model = Model()
        self.conn = DatabaseConnection().connect_dbs()
        self.cursor = self.conn.cursor()

    # This method will issue book by checking all the constraints
    # Here flag is raised to 1 if the member is a part of library else it will be raised to 0
    # All the Exceptions are taken care of
    def issue_book(self):
        flag = 0
        self.st_id = raw_input("Please enter the student ID :")
        self.cursor.execute(self.model.retrieve_data_using_select("student"))
        student_out = self.cursor.fetchall()
        for record in student_out:
            if self.st_id in record:
                flag = 1
                self.cards = record[7]
                break

        if flag == 1:
            print "Record Found"
        else:
            print "Record Not Found So book cannot be issued!"

        if flag == 1:
            flag = 0
            if self.cards > 0:

                self.book_id = raw_input("Please Provide the Book Id of book to be issued: ")
                self.cursor.execute(self.model.retrieve_data_using_select("book"))
                book_out = self.cursor.fetchall()
                for record in book_out:
                    if self.book_id in record:
                        self.instances = record[3]
                        if self.instances > 0:
                            flag = 1

                if flag == 1:
                    print "Book available! Wait issuing a copy for you!"
                    try:
                        self.cards -= 1
                        self.instances -= 1
                        self.cursor.execute("""UPDATE STUDENT SET cards=%s where st_id =%s""",
                                            (self.cards, self.st_id))

                        self.cursor.execute("""UPDATE Book SET available=%s where book_id =%s""",
                                            (self.instances, self.book_id))

                        issue_date = datetime.date.today()
                        due_date = issue_date + datetime.timedelta(15)

                        self.cursor.execute(
                            self.model.insert_new_entry("book_loan", st_id=self.st_id, book_id=self.book_id,
                                                        issue_date=issue_date, due_date=due_date))
                        self.conn.commit()

                        print """Book Issued Successfully!!\n\nPlease Note: Write current date
                        in backside of book and submit within 15 days fine Rs. 1 for each day 
                        after 15 days period"""



                    except Exception as e:
                        self.conn.rollback()
                        print "Book not issued due to following error!!"
                        print e


                else:
                    print "Book is not available So book cannot be issued!"

            else:
                print "Sorry ! Student does not have available cards for book issue."
