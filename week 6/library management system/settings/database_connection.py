# This will create a database connection for all files in the
# Library Management System

import pymysql
from dbsettings import connection_properties


class DatabaseConnection:

    def connect_dbs(self):
        self.econ = pymysql.connect(**connection_properties)
        return self.econ

