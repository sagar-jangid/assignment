# This class will take care of all the book return related concepts

import datetime
from settings.database_connection import DatabaseConnection
from models.model import Model


class ReturnBook(DatabaseConnection):
    # This method will initialize all the required variables
    def __init__(self):
        self.book_id = ''
        self.st_id = ''
        self.instances = 0
        self.cards = 0
        self.model = Model()
        self.connection_object = DatabaseConnection().connect_dbs()
        self. cursor = self.connection_object.cursor()

    # This method will return book by checking all the constraints
    # Here flag is raised to 1 if the member is a part of library else it will be raised to 0
    # Here the flag_loan is raised to one if the member has issued the entered book from library else will be 0
    # All the Exceptions are taken care of
    def return_book(self):
        flag = 0
        self.st_id = raw_input("Please enter the student ID :")
        self.cursor.execute(self.model.retrieve_data_using_select("student"))
        student_out = self.cursor.fetchall()
        for record in student_out:
            if self.st_id in record:
                flag = 1
                self.cards = record[7]
                break

        if flag == 1:
            print "Record Found"
        else:
            print "Record Not Found So book cannot be returned!"

        if flag == 1:
            flag = 0
            flag_loan = 0
            self.book_id = raw_input("Please Provide the Book Id of book to be returned: ")
            self.cursor.execute(self.model.retrieve_data_using_select("book"))
            book_out = self.cursor.fetchall()
            for record in book_out:
                if self.book_id in record:
                    self.instances = record[3]
                    flag = 1

            self.cursor.execute(self.model.retrieve_data_using_select("book_loan"))
            book_loan = self.cursor.fetchall()
            for loan_record in book_loan:
                if self.book_id in loan_record and self.st_id in loan_record:
                    flag_loan = 1
                    req_record = loan_record

            if flag == 1:

                if flag_loan == 1:

                    if req_record[4] == None:

                        print "Depositing Book For You!"
                        try:
                            self.cards += 1
                            self.instances += 1
                            today = datetime.date.today()
                            self.cursor.execute("SELECT due_date FROM book_loan WHERE st_id =%s and book_id =%s",
                                                (self.st_id, self.book_id))
                            due_date = self.cursor.fetchone()
                            if today > due_date[0]:
                                self.days = (today - due_date[0]).days
                                fine = self.days - 15
                                print "\n\nFine has to deposited Rs. ", fine

                            self.cursor.execute("""UPDATE STUDENT SET cards=%s where st_id =%s""",
                                                (self.cards, self.st_id))

                            self.cursor.execute("""UPDATE Book SET available=%s where book_id =%s""",
                                                (self.instances, self.book_id))

                            self.cursor.execute(
                                """UPDATE Book_loan SET return_date=%s where book_id =%s and st_id = %s""",
                                (today, self.book_id, self.st_id))
                            self.connection_object.commit()
                            print "BOOK RETURNED SUCCESSFULLY"

                        except Exception as e:
                            self.connection_object.rollback()
                            print "Book Not returned due to following error!!"
                            print e
                    else:
                        print" You already returned the book!!"

                else:
                    print "You Haven't issued the book."


            else:
                print "This Book Id is Incorrect as no book is available with the provided Id"

        else:
            print "Sorry ! Student is not registered in library."
