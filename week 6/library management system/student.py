# This class will keep track of the members of the library
# This class consist of the methods to cope up with all student data

from settings.database_connection import DatabaseConnection
from models.model import Model
import csv


class Student(DatabaseConnection):

    # This will initialize all the variables and will create a database connection
    def __init__(self):
        self.student_id = ''
        self.fname = ''
        self.lname = ''
        self.age = 0
        self.gender = ''
        self.phone = ''
        self.email = ''
        self.model = Model()
        self.conn = DatabaseConnection().connect_dbs()
        self.cursor = self.conn.cursor()

    # This will add or create new library member in database
    def insert_student_record(self):
        print "Create new student entry :"
        try:
            self.student_id = raw_input("Please provide student id: ")
            self.fname = raw_input("Please provide first name: ")
            self.lname = raw_input("Please provide last name: ")
            self.age = int(raw_input("Please provide age in years: "))
            self.gender = raw_input("Please provide gender of the student : ")
            self.phone = raw_input("Please provide phone no. of the student : ")
            self.email = raw_input("Please provide email id of the student : ")
        except Exception as e:
            print e

        try:
            self.cursor.execute(
                self.model.insert_new_entry("student", st_id=self.student_id, fname=self.fname, lname=self.lname,
                                            age=self.age, sex=self.gender, phone=self.phone, email=self.email))
            self.conn.commit()
            print "Entries Inserted Successfully!!"

        except Exception as e:
            self.conn.rollback()
            print "Entries not inserted due to following error!!"
            print e

    # This will show the record of all the students registered in library with the all attributes
    def retrieve_student_record(self):

        print "The following records are availaible: "
        try:
            self.cursor.execute(self.model.retrieve_data_using_select("student"))
            for satisfying in range(self.cursor.rowcount):
                results = self.cursor.fetchone()
                student_id = results[0]
                fname = results[1]
                lname = results[2]
                age = results[3]
                gender = results[4]
                phone = results[5]
                email = results[6]

                print "Student_Id = %s, Student_Name = %s %s, Student_Age = %d, Student_gender = %s, Student_Phone = %s, Student_Email = %s " % \
                      (student_id, fname, lname, age, gender, phone, email)

        except:
            print "Error: unable to fetch data"

    # This will remove the member from database
    def delete_student_record(self):

        id = raw_input("Please enter the Student_id of student which you want to delete: ")
        self.cursor.execute(self.model.retrieve_data_using_select("student",st_id =  id))
        try:
            if self.cursor.rowcount == 1:
                print "Deleting the required record!"
                self.cursor.execute(self.model.delete_specific_record("student",st_id = id))
                self.conn.commit()
                print "Successfully Deleted"
            else:
                print "Sorry ! Record not found!!"
                self.conn.rollback()

        except Exception as e:
            print e

    # This will update the record of the particular member in database
    def update_student_record(self):
        id = raw_input("Please enter the Student_id of student which you want to update: ")
        self.cursor.execute(self.model.retrieve_data_using_select("student", st_id = id))

        try:
            if self.cursor.rowcount == 1:
                fname = raw_input("Please provide first name: ")
                lname = raw_input("Please provide last name: ")
                age = int(raw_input("Please provide age in years: "))
                gender = raw_input("Please provide gender of the student : ")
                phone = raw_input("Please provide phone no. of the student : ")
                email = raw_input("Please provide email id of the student : ")
                self.cursor.execute("""UPDATE STUDENT SET fname=%s,lname=%s,
                                                age=%s,sex=%s,phone=%s,email=%s where st_id =%s""",
                                    (fname, lname, age, gender, phone, email, id))
                self.conn.commit()
                print "Entries Inserted Successfully!!"

            else:
                print "Sorry ! Record not found!!"


        except Exception as e:
            self.conn.rollback()
            print "Entries not updated due to following error!!"
            print e

    # This will add multiple records to database supplied through a input csv file from user
    def insert_bulk_student_record(self):
        try:
            print "please provide file name: "
            file_input = raw_input()
            data_file = open(file_input, 'r')
            data = csv.reader(data_file)
        except Exception as e:
            print "File cannot be opened due to ", e

        try:
            for row in data:
                row = tuple(row)

                self.cursor.execute(
                    """INSERT INTO student (st_id, fname, lname, age, sex, phone, email) VALUES (%s, %s, %s, %s, %s, %s, %s)""",
                    row)

            #     self.cursor.executemany("""INSERT INTO student (st_id, fname, lname, age, sex, phone, email) VALUES (%s, %s, %s, %s, %s, %s, %s)""",[
            #     ('SI004', 'Neha', 'Jnagid', 21, 'F', 13245678, 'neha@abc.com'),
            #     ('SI005', 'Manish', 'Tripathi', 28, 'M', 123245678, 'manish@abc.com'),
            #     ('SI006', 'Mohit', 'Agrawal', 22, 'M', 1324678, 'mohit@abc.com'),
            #     ('SI007', 'Himanshi', 'Singh', 21, 'F', 1325678, 'himanshi@abc.com'),
            #     ('SI008', 'Divya', 'Asija', 21, 'F', 1324578, 'divya@abc.com'),
            #     ('SI009', 'Garima', 'Arora', 21, 'F', 1324568, 'garima@abc.com'),
            #     ('SI010', 'Harsh', 'Kumawat', 22, 'M', 3245678, 'harsh@abc.com'),
            #     ('SI011', 'Aditya', 'Kaul', 22, 'M', 1245678, 'aditya@abc.com')
            # ])
            self.conn.commit()
            print" Values Successfully Inserted!!"

        except Exception as e:
            self.conn.rollback()
            print "Entries not inserted due to following error!!"
            print e

    # This method will show that how many cards are availaible for book issue to particular student
    def show_remaining_usable_cards(self):
        print "Enter your student id : "
        st_id = raw_input()
        try:
            self.cursor.execute(self.model.retrieve_data_using_select("student", st_id =  st_id))
            result = self.cursor.fetchone()
            return result[7]

        except Exception as e:
            print "Error: unable to fetch data"
