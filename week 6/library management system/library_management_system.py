# This is main class that will provide the all basic functionality of Library Management Systems
# All the other modules are imported here to provide full library functionality

from student import Student
from book import Book
from issue_book import IssueBook
from submit_book import ReturnBook
from display_issued_books import DisplayIssuedBooks
from settings.database_connection import DatabaseConnection


class LibraryManagementSystem:
    # This will initialize the all required variables.
    def __init__(self):
        self.student = Student()
        self.book = Book()
        self.issue_book = IssueBook()
        self.return_book = ReturnBook()
        self.display_issued_books = DisplayIssuedBooks()
        self.database_connection = DatabaseConnection().connect_dbs()

    # This method is responsible for calling all the functionalities
    def call_functionalities(self):

        print "\t\tMENU"
        print "\t1.CREATE STUDENT RECORD"
        print "\t2.DISPLAY ALL STUDENTS RECORD"
        print "\t3.UPDATE SPECIFIC STUDENT RECORD "
        print "\t4.DELETE STUDENT RECORD"
        print "\t5.INSERT MULTIPLE STUDENT RECORDS BY CSV FILE"
        print "\t6.CREATE BOOK "
        print "\t7.DISPLAY ALL BOOKS "
        print "\t8.UPDATE SPECIFIC BOOK "
        print "\t9.DELETE BOOK "
        print "\t10.INSERT MULTIPLE BOOK RECORDS BY CSV FILE"
        print "\t11.ISSUE BOOK"
        print "\t12.RETURN BOOK "
        print "\t13.CHECK FOR AVAILABLE CARDS: "
        print "\t14.CHECK FOR AVAILABLE INSTANCES OF BOOK "
        print "\t15.DISPLAY BOOKS ISSUED TO PARTICULAR STUDENT "
        print "\t\tEnter quit to exit!!  "
        while True:
            print "Please Enter Your Choice: "
            ch = raw_input()
            if ch == '1':
                self.student.insert_student_record()

            elif ch == '2':
                self.student.retrieve_student_record()

            elif ch == '3':
                self.student.update_student_record()

            elif ch == '4':
                self.student.delete_student_record()

            elif ch == '5':
                self.student.insert_bulk_student_record()

            elif ch == '6':
                self.book.insert_book_record()

            elif ch == '7':
                self.book.retrieve_book_record()

            elif ch == '8':
                self.book.update_book_record()

            elif ch == '9':
                self.book.delete_book_record()

            elif ch == '10':
                self.book.insert_bulk_book_record()

            elif ch == '11':
                self.issue_book.issue_book()

            elif ch == '12':
                self.return_book.return_book()

            elif ch == '13':
                print self.student.show_remaining_usable_cards(), "cards available!"

            elif ch == '14':
                print self.book.show_available_instances_of_books(), "instances of book are available"

            elif ch == '15':
                self.display_issued_books.display_issued_books()

            elif ch == 'quit':
                self.database_connection.close()
                print "Thank you for using the program!! "
                exit(0)

            else:
                print "Please enter a valid choice !!"


if __name__ == '__main__':
    library_management_system = LibraryManagementSystem()
    library_management_system.call_functionalities()
