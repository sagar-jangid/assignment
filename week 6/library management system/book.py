# This will keep track of available books and provide
# full functionality including adding updating and deleting books

from settings.database_connection import DatabaseConnection
from models.model import Model
import csv


class Book(DatabaseConnection):
    # This will initialize all the variables and will create a database connection
    def __init__(self):
        self.book_id = ''
        self.title = ''
        self.author_name = ''
        self.instances = 0
        self.model = Model()
        self.conn = DatabaseConnection().connect_dbs()
        self.cursor = self.conn.cursor()

    # This will add or create new book in database
    def insert_book_record(self):
        print "Create new book entry :"
        self.book_id = raw_input("Please provide book id: ")
        self.title = raw_input("Please provide book title: ")
        self.author_name = raw_input("Please provide author name: ")
        self.instances = int(raw_input("Please provide no. of available instances of book: "))

        try:

            self.cursor.execute(
                self.model.insert_new_entry_using_args("book", self.book_id, self.title, self.author_name,
                                                       self.instances))
            self.conn.commit()
            print "Entries Successfully Inserted"

        except Exception as e:
            self.conn.rollback()
            print "Entries not inserted due to following error!!"
            print e

    # This will show all the books available in library with the all attributes
    def retrieve_book_record(self):

        print "The following books are availaible: "
        try:
            self.cursor.execute(self.model.retrieve_data_using_select("book"))
            print "Total rows satisfying Query are: ", self.cursor.rowcount
            for satisfying in range(self.cursor.rowcount):
                results = self.cursor.fetchone()
                book_id = results[0]
                book_title = results[1]
                book_author = results[2]
                book_instance = results[3]

                print "Book_Id = %s, Book_Title = %s, Author_Name = %s, Available = %d" % \
                      (book_id, book_title, book_author, book_instance)

        except:
            print "Error: unable to fecth data"

    # This will remove the book from database
    def delete_book_record(self):

        id = raw_input("Please enter the Book_id of Book which you want to delete: ")
        self.cursor.execute(self.model.retrieve_data_using_select("book", book_id = id))
        try:
            if self.cursor.rowcount == 1:
                print "Deleting the required record!"
                self.cursor.execute(self.model.delete_specific_record("book", book_id = id))
                self.conn.commit()
                print "Successfully Deleted"
            else:
                print "Sorry ! Record not found!!"
                self.conn.rollback()

        except Exception as e:
            print e

    # This will update the book from database
    def update_book_record(self):
        id = raw_input("Please enter the book_id of book which you want to update: ")
        self.cursor.execute(self.model.retrieve_data_using_select("book", book_id = id))

        try:
            if self.cursor.rowcount == 1:
                title = raw_input("Please provide book title: ")
                author_name = raw_input("Please provide author name: ")
                instances = int(raw_input("Please provide no. of available instances of book: "))
                self.cursor.execute("""UPDATE BOOK SET title=%s,author_name=%s,available=%s where book_id =%s""",
                                    (title, author_name, instances, id))
                self.conn.commit()
                print "Entries Successfully Inserted"

            else:
                print "Sorry ! Record not found!!"


        except Exception as e:
            self.conn.rollback()
            print "Entries not updated due to following error!!"
            print e

    # This will add multiple records to database supplied through a input csv file from user
    def insert_bulk_book_record(self):
        try:
            print "please provide file name: "
            file_input = raw_input()
            data_file = open(file_input, 'r')
            data = csv.reader(data_file)
        except Exception as e:
            print "File cannot be opened due to ", e

        try:

            for row in data:
                row = tuple(row)

                self.cursor.execute(
                    """INSERT INTO book (book_id, title, author_name,available) VALUES ( %s, %s, %s, %s)""",
                    row)

            self.conn.commit()
            print" Book Details Successfully Inserted!!"

        except Exception as e:
            self.conn.rollback()
            print "Details not inserted due to following error!!"
            print e

    # This will show that how many copies of a particular book are available
    def show_available_instances_of_books(self):
        print "Enter your book id : "
        book_id = raw_input()
        try:
            self.cursor.execute(self.model.retrieve_data_using_select("book",book_id =  book_id))
            result = self.cursor.fetchone()
            return result[3]

        except Exception as e:
            print "Error: unable to fetch data"
