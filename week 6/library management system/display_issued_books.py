# This class will return the name of books issued currently by student

from settings.database_connection import DatabaseConnection


class DisplayIssuedBooks(DatabaseConnection):
    # This will initialize the required variables
    def __init__(self):
        self.book_id = ''
        self.st_id = ''
        self.connection_object = DatabaseConnection().connect_dbs()
        self.cursor = self.connection_object.cursor()

    # This method will return the name of books that the student is having currently
    # Here the flag is raised to 1 if the student has some books issued from library and that are not return till date
    def display_issued_books(self):

        self.st_id = raw_input("Please Enter ST_ID: ")

        try:
            flag = 0
            self.cursor.execute("SELECT * FROM Book_loan")
            book_loan = self.cursor.fetchall()

            for loan_record in book_loan:
                if self.st_id in loan_record and loan_record[4] == None:
                    self.book_id = loan_record[1]
                    flag = 1
                    self.cursor.execute("SELECT TITLE FROM BOOK WHERE BOOK_ID = %s", (self.book_id))
                    print "You have issued the %s Book." % (self.cursor.fetchone())


        except Exception as e:
            print "RECORD NOT FOUND!!"
            print e

        if flag == 0:
            print "RECORD NOT FOUND!"
