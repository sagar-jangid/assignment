# Convert Multiple XML files to a single JSON file XML file is input[i].xml
# where 0 <= i <= n and JSON file is output.json

import xmltodict, json, os


class Xml2json:
    def __init__(self):
        try:
            os.remove('output.json')
            print "Previously created file will be deleted now!!"
            self.js = open('output.json', 'w')

        except:
            print("File Not exist!!")
            print"Creating File For You!!"
            self.js = open('output.json', 'w')

    def to_json(self, file):
        dict = []
        dict = xmltodict.parse(file)
        json.dumps(dict)
        self.js.write(json.dumps(dict) + "\n")
        print (" Please Check output.json file it is written successfully for %s." % (str(file.name)))

    def closefile(self):
        self.js.close()


if __name__ == '__main__':
    try:
        xml1 = open('input1.xml')
        xml2 = open('input2.xml')
        xml3 = open('input3.xml')
    except IOError:
        print "Sorry! Please Specify File path correctly"

    x = Xml2json()
    x.to_json(xml1)
    x.to_json(xml2)
    x.to_json(xml3)

    xml1.close()
    xml2.close()
    xml3.close()

    x.closefile()
