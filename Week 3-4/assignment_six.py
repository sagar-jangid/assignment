# A simple program to show inheritance

class Engine:
    def set_engine_power(self, power):
        self.power = power
        return power

    def set_fuel_dispenser(self, fuel_dispenser):
        self.fuel_dispenser = fuel_dispenser
        return fuel_dispenser

    def set_displacement(self, displacement):
        self.displacement = displacement
        return displacement

    def set_ignition(self, ignition ):
        self.ignition = ignition
        return ignition

    def start_engine(self):
        print ("start Bike Using Ignition")

    def stop_engine(self):
        print("Stop fuel supply\n\n")

    def slow_down_engine(self):
        print("decrease fuel supply along with gear down")

    def speed_up_engine_up(self):
        print ("increase Fuel amount and gear up")


class Bike(Engine):
    def set_manufacturer_for_bike(self, manufacturer):
        self.manufacturer = manufacturer
        return manufacturer

    def set_model_name_for_bike(self, model_number):
        self.model_number = model_number
        return model_number

    def set_price_for_bike(self, price):
        self.price = price
        return price
    

passion = Bike()
print " The Manufacturer of the Bike is :", passion.set_manufacturer_for_bike("Hero")
print " The Model of the Bike is :", passion.set_model_name_for_bike("Passion")
print " The Price of the Bike is :", passion.set_price_for_bike(48950)
print " The Power of the Engine is :", passion.set_engine_power("8000 rpm")
print " The Power of the fuel delivery is through :", passion.set_fuel_dispenser("Carburettor")
print " The displacement of the Engine is :", passion.set_displacement("97 cc")
print " The ignition system of the Engine is :", passion.set_ignition("Digital CDI")
passion.start_engine()
passion.speed_up_engine_up()
passion.slow_down_engine()
passion.stop_engine()

r15 = Bike()
print " \n\nThe Manufacturer of the Bike is :", r15.set_manufacturer_for_bike("Yamaha")
print " The Model of the Bike is :", r15.set_model_name_for_bike("YZF R15")
print " The Price of the Bike is :", r15.set_price_for_bike(125000)
print " The Power of the Engine is :", r15.set_engine_power("10000 rpm")
print " The Power of the fuel delivery is through :", r15.set_fuel_dispenser("Fuel Injection")
print " The displacement of the Engine is :", r15.set_displacement("155 cc")
print " The ignition system of the Engine is :", r15.set_ignition("Transistor Controlled Ignition")
r15.start_engine()
r15.speed_up_engine_up()
r15.slow_down_engine()
r15.stop_engine()
