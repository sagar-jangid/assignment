# Write a method to take list as an input and gives output as list of tuples.
# where each tuple will contain (index, value).
# example: input = [2,3,4,5,6]
# output = [(0,2), (1,3), (2,4), (3, 5), (4, 6)]


class ListOfTuples:

    # Constructor that takes a list and initialize an empty output list
    def __init__(self, input_list):
        self.input_list = input_list
        self.output_list = []

    # Method that will convert the list in required format
    def convert_list(self):
        for index, jndex in enumerate(self.input_list):
            self.output_list.append((index, jndex))
        return self.output_list


# Main Function


if __name__ == '__main__':
    input_list = [10, 1, 2, 3, 4, 5, 6]
    list_of_tuples = ListOfTuples(input_list)
    print list_of_tuples.convert_list()
